# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np
from mathutils import Vector

from .geometry_3D import rotate

def display_angle(alpha, beta, loc, scale=0.3, dv = np.array([0,10,0]), category="") :
    """Displays angle (usually as seen through the lens of a camera), 
    by rotating a base vector by an angle.
    
    - alpha : angle for Z rotation
    - beta : angle for X rotation
    - loc : origin of the angle
    - scale : scale of the empty
    - dv : base vector, default = np.array([0,10,0])"""

    loc = np.array(loc)

    direction = dv
    if alpha != 0 :
        direction = rotate(direction, alpha, "Z")
    if beta != 0 :
        direction = rotate(direction, beta, "X")
    p = loc+direction
    create_empty(p, type="PLAIN_AXES", scale=scale, category=category)

def get_preview_collection() :
    """Returns PREVIEW_COL collection if it exists, creates it otherwise."""
    
    if "COL_PREVIEW" not in bpy.data.collections :
        collection = bpy.data.collections.new("COL_PREVIEW")
        bpy.context.scene.collection.children.link(collection)
        return collection
    else :
        return bpy.data.collections['COL_PREVIEW']


def create_empty(location, scale=0.3, type='CIRCLE', rotation=None, category="") :
    """Creates empty at specified location.
    
    - location : location of the empty
    - scale : scale of the empty
    - rotation : rotation of the empty
    - type : display type of the empty (default = 'CIRCLES')
    """

    blacklist = [
        "", 
        # "cam_corner_projections",
        "frame_FOV_angles",
        "max_angle",
        "min_angle",
    ]

    if category in blacklist :
        return
    
    # print('Creating empty at (%.2f, %.2f, %.2f)'%(location[0], location[1], location[2]))
    
    collection = get_preview_collection()
    
    empty = bpy.data.objects.new("POSITION_PREVIEW", None)
    empty.empty_display_type = type

    empty.location = location
    empty.scale = (scale, scale, scale)

    if rotation is not None :
        empty.rotation_euler = rotation

    collection.objects.link(empty)

def destroy_empties() :
    """Destroys all preview empties created."""

    if "COL_PREVIEW" not in bpy.data.collections :
        return
    
    collection = bpy.data.collections['COL_PREVIEW']

    for o in collection.objects:
        if "POSITION_PREVIEW" in o.name :
            bpy.data.objects.remove(o, do_unlink=True)
    
    bpy.data.collections.remove(collection)