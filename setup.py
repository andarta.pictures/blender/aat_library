from setuptools import setup, find_packages

setup(
    name='aat_library',
    version='1.0',
    packages=find_packages(),
    py_modules=['anim_utils', 'general_utils', 'gp_utils'],  # Replace with your module names
    install_requires=[],
)
